﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper
{
    public static void SwapTwobjectsInList(IList list, int indexA, int indexB)
    {
        var temporary = list[indexA];
        list[indexA] = list[indexB];
        list[indexB] = temporary;
    }
}
