﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.IO;
using UnityEngine.EventSystems;

public class ScreenshotPhoto : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private Image _image;

    private Sprite sprite;

    private string path;

    public void SetLocationPath(string path)
    {
        this.path = path;

        _image.sprite = LoadSprite(path);
    }

    private Sprite LoadSprite(string path)
    {
        if (string.IsNullOrEmpty(path)) return null;
        if (System.IO.File.Exists(path))
        {
            byte[] bytes = System.IO.File.ReadAllBytes(path);
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            return sprite;
        }
        return null;
    }

    public void SetSprite(Sprite sprite)
    {
        _image.sprite = sprite;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ScreenshotImage.Instance.gameObject.SetActive(true);
        Texture2D texture = new Texture2D(1, 1);
        ScreenshotImage.Instance.gameObject.GetComponent<Image>().material.mainTexture = this._image.mainTexture;

    }
}
