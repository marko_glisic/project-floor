using UnityEngine;
using UnityEngine.EventSystems;

public class ScreenshotImage : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private CanvasGroup canvasGroup;

    public GameObject screenshotImage;

    public static ScreenshotImage Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void Hide()
    {
        canvasGroup.alpha = 0f; //this makes everything transparent
        canvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void Show()
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }

    void Start()
    {
        screenshotImage.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        screenshotImage.SetActive(false);
    }
}