﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Screenshot : MonoBehaviour
{
    [SerializeField]
    private RawImage _image;

    private Texture2D _tex2D;

    public void DisableOnSuccess()
    {
        gameObject.SetActive(false);
    }

    public void SetScreenshotTexture(Texture2D texture)
    {
        _image.texture = texture;
        _tex2D = texture;
    }

    public Sprite GetSprite()
    {
        return Sprite.Create(_tex2D, new Rect(0, 0, _tex2D.width, _tex2D.height), new Vector2(0.5f, 0.5f));
    }
}
