﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sebastian.Geometry;

public class TouchPoint : MonoBehaviour
{
    [SerializeField]
    private Color _unselectedColor;

    [SerializeField]
    private Color _selectedColor;

    private Vector3 _startScale;
    private Vector3 _newScale;
    private Collider _collider;

    [SerializeField]
    private int _listOrderNumber;

    private void Awake()
    {
        _startScale = transform.localScale;
        _collider = GetComponent<Collider>();
    }

    private void Start()
    {
        _newScale = new Vector3(_startScale.x + 0.01f, _startScale.y, _startScale.z + 0.01f);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag == "Pointer")
        {
            OnPointerEnter();
        }
    }
    
    private void OnTriggerStay(Collider other)
    {
        if(other.transform.tag == "Pointer")
        {
            OnPointerEnter();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.transform.tag == "Pointer")
        {
            OnPointerExit();
        }
    }

    public void OnPointerEnter()
    {
        GetComponent<MeshRenderer>().material.color = _selectedColor;
        transform.localScale = _newScale;
        PlaneManager.Instance.SetPointBool(true);
        PlaneManager.Instance.SelectPoint(this);
    }

    public void OnPointerExit()
    {
        GetComponent<MeshRenderer>().material.color = _unselectedColor;
        transform.localScale = _startScale;
        PlaneManager.Instance.SetPointBool(false);
        PlaneManager.Instance.UnselectPoint();
    }

    public void SetOrderNumberInList(int number)
    {
        _listOrderNumber = number;
    }

    public int GetOrderNumber()
    {
        return _listOrderNumber;
    }

    public void EnableCollider()
    {
        _collider.enabled = true;
    }

    public void DisableCollider()
    {
        _collider.enabled = false;
    }
}
