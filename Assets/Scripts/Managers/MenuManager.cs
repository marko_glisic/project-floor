﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public static MenuManager Instance;

    public static bool isAnyMenuOpen = false;

    // Menus
    public GalleryMenu GalleryMenu;
    public MeasureMenu MeasureMenu;

    [SerializeField]
    private List<Menu> menus = new List<Menu>();

    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Gets number of active menus
    /// </summary>
    public void GetActiveMenus()
    {
        List<Menu> activeMenus = new List<Menu>();
        activeMenus.Clear();

        // Stores any active menu to the list
        foreach (var menu in menus)
        {
            if (menu.isOpen == true)
            {
                activeMenus.Add(menu);
            }
        }

        // Checks if any menu is open
        if (activeMenus.Count <= 0)
            StartCoroutine(SetMenusOpenFalseDelayed());
        else
            isAnyMenuOpen = true;
    }

    /// <summary>
    /// Sets menues disabled by small delay
    /// Prevents clicking through menus in AR
    /// </summary>
    /// <returns></returns>
    private IEnumerator SetMenusOpenFalseDelayed()
    {
        yield return new WaitForSeconds(.5f);

        isAnyMenuOpen = false;
    }

    /// <summary>
    /// Sets active state of each menu
    /// </summary>
    /// <param name="value"></param>
    public void SetAllMenusActive(bool value)
    {
        foreach (var menu in menus)
        {
            menu.gameObject.SetActive(value);

            if(!value)
            {
                
            }
        }
    }
}