﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Sebastian.Geometry;

public class PlaneManager : MonoBehaviour
{
    public static PlaneManager Instance;

    public TextureSlider TextureSlider;

    private bool isOverPoint = false;
    private bool isOverLine = false;

    public TouchPoint SelectedPoint = null;
    public Line SelectedLine = null;

    private void Awake()
    {
        Instance = this;
    }

    public void SetPointBool(bool value)
    {
        isOverPoint = value;
    }

    public void SetLineBool(bool value)
    {
        isOverLine = value;
    }

    public bool IsOverPoint()
    {
        return isOverPoint;
    }

    public bool IsOverLine()
    {
        return isOverLine;
    }

    public void SelectPoint(TouchPoint point)
    {
        SelectedPoint = point;
    }

    public void UnselectPoint()
    {
        SelectedPoint = null;
    }

    public void SelectLine(Line line)
    {
        SelectedLine = line;
    }
}
