﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainManager : MonoBehaviour
{

    [SerializeField]
    private CategoriesMenu _categoriesMenu;

    [SerializeField]
    private SideMenu _sideMenu;


    [SerializeField]
    private TileMenu _tileMenu;

    [SerializeField]
    private GalleryMenu _galeryMenu;

    [SerializeField]
    private MeasureMenu _measureMenu;
    public static MainManager Instance;
    
    [SerializeField]
    private MobileDebugger MobileDebugger;

    [SerializeField]
    private Screenshot Screenshot;

    [SerializeField]
    private ScreenshotImage _screenshotImage;

    private void Awake()
    {
        Instance = this;
        Application.targetFrameRate = 60;
    }

    public void PrintDebugMessage(string msg)
    {
        MobileDebugger.PrintDebugMessage(msg);
    }

    public void RestartCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        MenuManager.isAnyMenuOpen = false;
        
    }

    public void TakeScreenshot()
    {
        StartCoroutine(CaptureSceen());
    }

    private IEnumerator CaptureSceen()
    {
        // Hide all menus on screen capture
        _categoriesMenu.Hide();
        _sideMenu.Hide();
        _tileMenu.Hide();
        _galeryMenu.Hide();
        _measureMenu.Hide();

        yield return new WaitForSeconds(.5f);

        // Set current time and create a file name
        string timeTaken = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss");
        string fileName = "Screenshot" + timeTaken + ".png";
        string path = fileName;

        // Capture screenshot with path
        ScreenCapture.CaptureScreenshot(path);

        // Display taken screenshot
        RenderTexture renderTexture = Camera.main.targetTexture;
        Texture2D renderResult = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        Rect rect = new Rect(0, 0, Screen.width, Screen.height);
        renderResult.ReadPixels(rect, 0, 0);
        renderResult.Apply();

        yield return new WaitForEndOfFrame();
        Screenshot.gameObject.SetActive(true);

        Screenshot.SetScreenshotTexture(renderResult);
        _categoriesMenu.Show();
        _sideMenu.Show();
        _tileMenu.Show();
        _galeryMenu.Show();
        _measureMenu.Show();

        MenuManager.Instance.SetAllMenusActive(true);
        MenuManager.Instance.GalleryMenu.LoadScreenshot(path);
    }

    public Sprite GetScreenshotSprite()
    {
        return Screenshot.GetSprite();
    }
}
