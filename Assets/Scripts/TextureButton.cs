﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureButton : MonoBehaviour
{
    [SerializeField]
    private Texture2D _mainTexture;

    [SerializeField]
    private Texture2D _normal;

    [SerializeField]
    private Texture2D _metallic;

    [SerializeField]
    private float _tileWidth;

    public void SelectTexture()
    {
        PlaneManager.Instance.TextureSlider.SelectTexture(_mainTexture, _normal, _metallic, _tileWidth);
    }
}
