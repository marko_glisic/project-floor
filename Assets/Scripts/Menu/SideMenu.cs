﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SideMenu : Menu
{
    [SerializeField]
    private GameObject openMenuButton;

    [Header("Top buttons")]

    [SerializeField]
    private Button _tepisiButton;

    [SerializeField]
    private Button _laminatiButton;

    [SerializeField]
    private Button _plociceButton;

    [SerializeField]
    private GameObject _selector;

    [Header("Mid buttons")]

    [SerializeField]
    private Button galerijaButton;

    [SerializeField]
    private Button bibliotekaButton;

    [SerializeField]
    private GameObject _midSelector;

    [Header("Bottom buttons")]
    [SerializeField]
    private Button izracunajButton;

    [SerializeField]
    private Button fotografisiButton;

    [SerializeField]
    private GameObject bottomSelector;

    [SerializeField]
    private CanvasGroup canvasGroup;


    public void Hide()
    {
        canvasGroup.alpha = 0f; //this makes everything transparent
        canvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void Show()
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }

    public override void OpenMenu()
    {
        if(isOpen == true) 
        {
            GalleryMenu.Instance.Close();
        }
        base.OpenMenu();

    }

    public void ShowOpenMenuButton()
    {
        openMenuButton.gameObject.SetActive(true);
    }

    public void SetSelector(Button button)
    {
        _selector.transform.SetParent(button.transform);
        _selector.transform.SetAsFirstSibling();
        _selector.transform.localPosition = new Vector3(0, 0, 0);
        _selector.GetComponent<Animator>().Play("OnSelected");
    }

    public void SetMidSelector(Button button)
    {
        _midSelector.transform.SetParent(button.transform.parent);
        _midSelector.transform.SetAsFirstSibling();
        _midSelector.transform.localPosition = new Vector3(0, 0, 0);
        _midSelector.GetComponent<Animator>().Play("OnSelected");
    }

    public void SetBottomSelector(Button button)
    {
        bottomSelector.transform.SetParent(button.transform.parent);
        bottomSelector.transform.SetAsFirstSibling();
        bottomSelector.transform.localPosition = new Vector3(0, 0, 0);
        bottomSelector.GetComponent<Animator>().Play("OnSelected");
    }
}
