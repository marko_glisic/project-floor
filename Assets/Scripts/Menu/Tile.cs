﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public float tileWidth;
    public Texture mainTexture;
    public Texture normalTexture;
    public Texture agmTexture;

    public void SelectTexture()
    {
        PlaneGenerator.Instance.CustomPlaneMesh.SetTexture(tileWidth, mainTexture, normalTexture, agmTexture);
    }
}
