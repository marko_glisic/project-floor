﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoriesMenu : Menu
{
    [SerializeField]
    private List<Text> textList = new List<Text>();

    [SerializeField]
    private All _all;
    [SerializeField]
    private Emmon _emmon;
    [SerializeField]
    private Kanjiza _kanjiza;
    [SerializeField]
    private Marezi _marezi;
    [SerializeField]
    private Tarkett _tarkett;
    [SerializeField]
    private Zorka _zorka;
    [SerializeField]
    private Iberia _iberia;
    public Text SelectedCategory;

    [SerializeField]
    private CanvasGroup canvasGroup;


    public void Hide()
    {
        canvasGroup.alpha = 0f; //this makes everything transparent
        canvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void Show()
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }

    public override void OpenMenu()
    {
        base.OpenMenu();
        _all.Show();
        _emmon.Hide();
        _kanjiza.Hide();
        _marezi.Hide();
        _tarkett.Hide();
        _zorka.Hide();
        _iberia.Hide();
    }

    public void SelectCategory(Text category)
    {
        if (!textList.Contains(category))
        {
            textList.Add(category);
        }


        foreach (var txt in textList)
        {
            txt.fontSize = 14;
        }

        switch (category.text)
        {
            case "CBE":
                _all.Show();
                _emmon.Hide();
                _kanjiza.Hide();
                _marezi.Hide();
                _tarkett.Hide();
                _zorka.Hide();
                _iberia.Hide();
                break;
            case "ЗОРКА":
                _all.Hide();
                _emmon.Hide();
                _kanjiza.Hide();
                _marezi.Hide();
                _tarkett.Hide();
                _zorka.Show();
                _iberia.Hide();
                break;
            case "ТАРКЕТТ":
                _all.Hide();
                _emmon.Hide();
                _kanjiza.Hide();
                _marezi.Hide();
                _tarkett.Show();
                _zorka.Hide();
                _iberia.Hide();
                break;
            case "КАЊИЖА":
                _all.Hide();
                _emmon.Hide();
                _kanjiza.Show();
                _marezi.Hide();
                _tarkett.Hide();
                _zorka.Hide();
                _iberia.Hide();
                break;
            case "ЕНМОН":
                _all.Hide();
                _emmon.Show();
                _kanjiza.Hide();
                _marezi.Hide();
                _tarkett.Hide();
                _zorka.Hide();
                _iberia.Hide();
                break;
            case "МАРЕЗИ":
                _all.Hide();
                _emmon.Hide();
                _kanjiza.Hide();
                _marezi.Show();
                _tarkett.Hide();
                _zorka.Hide();
                _iberia.Hide();
                break;
            case "ИБЕРИА":
                _all.Hide();
                _emmon.Hide();
                _kanjiza.Hide();
                _marezi.Hide();
                _tarkett.Hide();
                _zorka.Hide();
                _iberia.Show();
                break;
            default:
                _all.Show();
                _emmon.Hide();
                _kanjiza.Hide();
                _marezi.Hide();
                _tarkett.Hide();
                _zorka.Hide();
                _iberia.Hide();
                break;
        }

        SelectedCategory = category;
        SelectedCategory.fontSize = 16;
    }

    public void Close()
    {
        isOpen = false;
        GetComponent<Animator>().Play("Close");
        MenuManager.Instance.GetActiveMenus();
    }

    public void Open()
    {
        isOpen = true;
        GetComponent<Animator>().Play("Open");
        MenuManager.Instance.GetActiveMenus();
        _all.Show();
        _emmon.Hide();
        _kanjiza.Hide();
        _marezi.Hide();
        _tarkett.Hide();
        _zorka.Hide();
        _iberia.Hide();

    }
}