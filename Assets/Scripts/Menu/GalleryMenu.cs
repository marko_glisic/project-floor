﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GalleryMenu : Menu
{
    [SerializeField]
    private ScreenshotPhoto _photo;

    [SerializeField]
    private Transform _container;

    [SerializeField]
    private int _maxPhotos = 100;

    private List<ScreenshotPhoto> photoList = new List<ScreenshotPhoto>();

    private string[] _files;

    [SerializeField]
    private CanvasGroup canvasGroup;


    public static GalleryMenu Instance;

    public void Hide()
    {
        canvasGroup.alpha = 0f; //this makes everything transparent
        canvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void Show()
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }

    private void Awake()
    {
        Instance = this;
        CreatePhotos();
    }

    private void Start()
    {
        LoadScreenshotsFromMemory();
    }

    public override void OpenMenu()
    {
        base.OpenMenu();
    }

    public void Close()
    {
        isOpen = false;
        GetComponent<Animator>().Play("Close");
        MenuManager.Instance.GetActiveMenus();
    }

    public void Open()
    {
        isOpen = true;
        GetComponent<Animator>().Play("Open");
        MenuManager.Instance.GetActiveMenus();
    }

    public void LoadScreenshotsFromMemory()
    {
        _files = Directory.GetFiles(Application.persistentDataPath + "/", "*.png");

        for (int i = 0; i < _files.Length; i++)
        {
            photoList[i].gameObject.SetActive(true);
            photoList[i].SetLocationPath(_files[i]);
        }
    }

    public void LoadScreenshot(string path)
    {
        for (int i = 0; i < _maxPhotos; i++)
        {
            if (!photoList[i].gameObject.activeInHierarchy)
            {
                photoList[i].gameObject.SetActive(true);
                photoList[i].SetSprite(MainManager.Instance.GetScreenshotSprite());
                break;
            }
        }
    }

    private void CreatePhotos()
    {
        for (int i = 0; i < _maxPhotos; i++)
        {
            ScreenshotPhoto photo = Instantiate(_photo) as ScreenshotPhoto;
            photo.transform.SetParent(_container);
            photo.transform.localScale = new Vector3(1, 1, 1);
            photo.gameObject.SetActive(false);
            photoList.Add(photo);
        }
    }

    public bool IsOpen()
    {
        return isOpen;
    }
}
