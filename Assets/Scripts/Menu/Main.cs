using UnityEngine;

public class Main : MonoBehaviour
{
    public static Main Instance;
    [SerializeField]
    private CanvasGroup canvasGroup;

    private void Awake()
    {
        Instance = this;
    }

    public void Hide()
    {
        canvasGroup.alpha = 0f; //this makes everything transparent
        canvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void Show()
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }
}
