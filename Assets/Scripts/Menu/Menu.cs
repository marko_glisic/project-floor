﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Menu : MonoBehaviour
{
    public bool isOpen = false;

    public virtual void OpenMenu()
    {
        if(isOpen)
        {
            isOpen = false;
            GetComponent<Animator>().Play("Close");
        }
        else
        {
            isOpen = true;
            GetComponent<Animator>().Play("Open");
        }

        MenuManager.Instance.GetActiveMenus();
    }
}
