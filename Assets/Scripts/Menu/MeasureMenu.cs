﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeasureMenu : Menu
{
    [SerializeField]
    private Text _measureText;
    [SerializeField]
    private CanvasGroup canvasGroup;


    public void Hide()
    {
        canvasGroup.alpha = 0f; //this makes everything transparent
        canvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void Show()
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }

    public override void OpenMenu()
    {
        base.OpenMenu();
    }

    public void CalculateSquareArea(List<TouchPoint> pointList)
    {
        _measureText.text = GetSquareAreaMeasure(pointList).ToString("F1") + "m\u00b2";
    }

    private float GetSquareAreaMeasure(List<TouchPoint> pointList)
    {
        float temp = 0;
        int i = 0 ;

        for(; i < pointList.Count ; i++)
        {
            if(i != pointList.Count - 1)
            {
                float mulA = pointList[i].transform.position.x * pointList[i+1].transform.position.z;
                float mulB = pointList[i+1].transform.position.x * pointList[i].transform.position.z;
                temp = temp + ( mulA - mulB );
            }
            else
            {
                float mulA = pointList[i].transform.position.x * pointList[0].transform.position.z;
                float mulB = pointList[0].transform.position.x * pointList[i].transform.position.z;
                temp = temp + ( mulA - mulB );
            }
        }

        temp *= 0.5f;
        return Mathf.Abs(temp);
    }
}
