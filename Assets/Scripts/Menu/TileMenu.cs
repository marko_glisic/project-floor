﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileMenu : Menu
{
    // [SerializeField]
    // private Rotate _rotator;
    
    [SerializeField]
    private CategoriesMenu _categoriesMenu;

    [SerializeField]
    private List<Tile> tileList = new List<Tile>();

    [SerializeField]
    private Text tileName;

    public Tile SelectedTile;

    [SerializeField]
    private CanvasGroup canvasGroup;


    public void Hide()
    {
        canvasGroup.alpha = 0f; //this makes everything transparent
        canvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void Show()
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }

    public override void OpenMenu()
    {
        if(isOpen)
        {
            isOpen = false;
            GetComponent<Animator>().Play("Close");
            _categoriesMenu.Close();
            MenuManager.Instance.GetActiveMenus();
        }
        else
        {
            isOpen = true;
            GetComponent<Animator>().Play("Open");
            _categoriesMenu.Open();
            MenuManager.Instance.GetActiveMenus();
        }
    }

    public void SelectTile(Tile tile){
        SelectedTile = tile;

        foreach(var _tile in tileList)
        {
            _tile.GetComponent<RectTransform>().sizeDelta = new Vector2(100, 100);
        }
        SelectedTile.GetComponent<RectTransform>().sizeDelta = new Vector2(120, 120);
        // OpenMenu();
    }

    public void SetTileName(string name)
    {
        tileName.text = name;
    }
    
}
