﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARSubsystems;
using Sebastian.Geometry;
using UnityEngine.EventSystems;

public class PlaneGenerator : MonoBehaviour
{
    public static PlaneGenerator Instance;

    [SerializeField]
    CustomLineRenderer CustomLineRenderer;

    [SerializeField]
    private TouchPoint _point;

    private List<TouchPoint> pointList = new List<TouchPoint>();
    [SerializeField] List<Line> lineList = new List<Line>();

    [SerializeField]
    private LayerMask _whatIsGround;

    public CustomPlaneMesh CustomPlaneMesh;

    [SerializeField]
    public Pointer Pointer;

    [SerializeField]
    private Line _line;

    [SerializeField]
    private Transform _linesContainer;

    [SerializeField]
    private Transform _pointsContainer;

    private int pointCounter = -1;

    private void Awake()
    {
        Instance = this;
        CustomLineRenderer.CreateLines(_linesContainer);
        CreatePoints();
    }

    private void Update()
    {
        SetPointer();
    }

    /// <summary>
    /// Sets point in real space and creates a custom mesh based on input data
    /// </summary>
    private void SetPointer()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(.5f, .5f, 0f));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _whatIsGround))
        {
            // Sets pointer to the ground
            Pointer.transform.position = hit.point;

            // Checks if pointer is not colliding with points and lines
            // Executes an event on click
            if (!PlaneManager.Instance.IsOverLine() && !PlaneManager.Instance.IsOverPoint())
            {
                // if (Input.GetMouseButtonUp(0) && !MenuManager.isAnyMenuOpen)
                if (Input.GetMouseButtonUp(0) && EventSystem.current.currentSelectedGameObject == null && !GalleryMenu.Instance.IsOpen())
                {
                    AddPoint(hit.point);
                    CustomPlaneMesh.transform.position = new Vector3(CustomPlaneMesh.transform.position.x, hit.point.y, CustomPlaneMesh.transform.position.z);
                    CustomPlaneMesh.transform.eulerAngles = new Vector3(0, 0, 0);
                    CustomPlaneMesh.SetVerticesData(new Vector3(hit.point.x, 0, hit.point.z));
                    CustomLineRenderer.DrawLines(pointList);
                }
            }
        }

        // Checks if pointer is over a point
        // On click event
        if (PlaneManager.Instance.IsOverPoint())
        {
            if (Input.GetMouseButtonUp(0) && EventSystem.current.currentSelectedGameObject == null && !GalleryMenu.Instance.IsOpen())
            {
                List<Vector3> vertecies = new List<Vector3>();
                vertecies.Clear();
                foreach (var point in pointList)
                {
                    if (point.gameObject.activeInHierarchy)
                    {
                        vertecies.Add(point.transform.position);
                    }
                }

                CustomPlaneMesh.RecalculateMesh(vertecies);
                MenuManager.Instance.MeasureMenu.CalculateSquareArea(pointList);
            }

            if (Input.GetMouseButton(0) && EventSystem.current.currentSelectedGameObject == null && !GalleryMenu.Instance.IsOpen())
            {
                TouchPoint selectedPoint = PlaneManager.Instance.SelectedPoint;
                selectedPoint.transform.position = new Vector3(hit.point.x, CustomPlaneMesh.transform.position.y + 0.0001f, hit.point.z);

                foreach (var line in CustomLineRenderer.lines)
                {
                    if (line.gameObject.activeInHierarchy)
                        line.UpdateMesh(CustomPlaneMesh.transform.position.y + 0.0001f);
                }

                foreach (var point in pointList)
                {
                    point.DisableCollider();
                    if (point == PlaneManager.Instance.SelectedPoint)
                    {
                        point.EnableCollider();
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                foreach (var point in pointList)
                {
                    point.EnableCollider();
                }
            }
        }

        if (PlaneManager.Instance.IsOverLine() && !PlaneManager.Instance.IsOverPoint() && !GalleryMenu.Instance.IsOpen())
        {
            if (Input.GetMouseButtonDown(0) && EventSystem.current.currentSelectedGameObject == null)
            {
                Line selectedLine = PlaneManager.Instance.SelectedLine;

                int orderNumber = pointList.IndexOf(selectedLine.GetPointB());

                for (int i = 0; i < pointList.Count; i++)
                {
                    if (!pointList[i].gameObject.activeInHierarchy)
                    {
                        pointList[i].transform.position = new Vector3(hit.point.x, CustomLineRenderer.transform.position.y + 0.001f, hit.point.z);
                        pointList[i].transform.SetSiblingIndex(orderNumber);
                        pointList[i].gameObject.SetActive(true);
                        break;
                    }
                }

                List<TouchPoint> newSortedList = new List<TouchPoint>();
                newSortedList.AddRange(_pointsContainer.GetComponentsInChildren<TouchPoint>());

                for (int i = 0; i < newSortedList.Count; i++)
                {
                    pointList[i] = newSortedList[i];
                }

                CustomLineRenderer.DrawLines(pointList);
            }
        }

        MenuManager.Instance.MeasureMenu.CalculateSquareArea(pointList);
    }

    private void AddPoint(Vector3 position)
    {
        for (int i = 0; i < pointList.Count - 1; i++)
        {
            if (!pointList[i].gameObject.activeInHierarchy)
            {
                pointList[i].gameObject.SetActive(true);
                pointList[i].transform.SetParent(_pointsContainer);
                pointList[i].transform.position = new Vector3(position.x, CustomLineRenderer.transform.position.y + 0.001f, position.z);
                break;
            }
        }
    }

    private void CreatePoints()
    {
        for (int i = 0; i < 50; i++)
        {
            TouchPoint point = Instantiate(_point) as TouchPoint;
            point.transform.SetParent(_pointsContainer);
            point.gameObject.SetActive(false);
            pointList.Add(point);
        }
    }

    public void RemovePoint()
    {
        for (int i = pointList.Count - 1; i >= 0; i--)
        {
            if (pointList[i].gameObject.activeInHierarchy)
            {
                pointList[i].gameObject.SetActive(false);
                break;
            }
        }


        CustomLineRenderer.DrawLines(pointList);

        List<Vector3> vertecies = new List<Vector3>();
        vertecies.Clear();
        foreach (var point in pointList)
        {
            if (point.gameObject.activeInHierarchy)
            {
                vertecies.Add(point.transform.position);
            }
        }

        CustomPlaneMesh.RecalculateMesh(vertecies);
        MenuManager.Instance.MeasureMenu.CalculateSquareArea(pointList);

    }
}
