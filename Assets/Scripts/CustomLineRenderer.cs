﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomLineRenderer : MonoBehaviour
{
    [SerializeField]
    private int _maxLines = 50;

    [SerializeField]
    private Line _line;

    private List<TouchPoint> _pointList;

    public List<Line> lines = new List<Line>();

    public Line LoopLine;


    public List<TouchPoint> _activePoints = new List<TouchPoint>();

    /// <summary>
    /// Creates lines at start
    /// </summary>
    /// <param name="parent"></param>
    public void CreateLines(Transform parent)
    {
        for (int i = 0; i < _maxLines; i++)
        {
            Line line = Instantiate(_line) as Line;
            line.transform.SetParent(parent);
            line.transform.position = new Vector3(0, 0.001f, 0);
            line.gameObject.SetActive(false);
            lines.Add(line);
        }
    }

    /// <summary>
    /// Draws lines for each point vector in real space
    /// </summary>
    /// <param name="pointList"></param>
    public void DrawLines(List<TouchPoint> pointList)
    {
        _activePoints.Clear();
        foreach (var point in pointList)
        {
            if (point.gameObject.activeInHierarchy)
                _activePoints.Add(point);
        }

        for (int i = 0; i < pointList.Count; i++)
        {
            lines[i].gameObject.SetActive(false);
        }


        for (int i = 0; i < _activePoints.Count - 1; i++)
        {
            lines[i].gameObject.SetActive(true);
            lines[i].SetPosition(pointList[i].transform.position);

            lines[i].SetConnectingPoints(pointList[i], pointList[i + 1]);
            lines[i].UpdateMesh(pointList[i].transform.position.y);

            LoopLine = lines[_activePoints.Count - 1];

            LoopLine.gameObject.SetActive(true);
            LoopLine.SetPosition(_activePoints[_activePoints.Count - 1].transform.position);
            LoopLine.SetConnectingPoints(_activePoints[_activePoints.Count - 1], _activePoints[0]);
            LoopLine.UpdateMesh(_activePoints[_activePoints.Count - 1].transform.position.y);
        }

        _pointList = pointList;
    }
}