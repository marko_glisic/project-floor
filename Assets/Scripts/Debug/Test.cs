﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Test : MonoBehaviour
{
    public static Test Instance;

    [SerializeField] private CustomLineRenderer CustomLineRenderer;
    [SerializeField] private TouchPoint _point;
    [SerializeField] private DebugLine _debugLine;
    [SerializeField] private Line _selectedLine;
    [SerializeField] private DebugPlaneMesh DebugPlaneMesh;
    List<TouchPoint> newSortedList = new List<TouchPoint>();    

    [SerializeField]
    private List<TouchPoint> pointList = new List<TouchPoint>();
    private LayerMask _whatIsGround;
    private int orderNumber = -1;

    [SerializeField]
    private Transform _linesContainer;

    [SerializeField]
    private Transform _pointContainer;

    bool isOverPoint = false;

    private void Awake()
    {
        CustomLineRenderer.CreateLines(_linesContainer);
        CreatePoints();
    }

    private void Update()
    {
        SetPoint();
        UpdateMesh();
    }

    private void SetPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();

        if (Physics.Raycast(ray, out hit))
        {
            if(hit.transform.tag == "Point" && Input.GetMouseButton(0))
            {
                hit.transform.position = hit.point;
                return;
            }
            if(hit.transform.tag == "Line" && Input.GetMouseButtonDown(0))
            {
                _selectedLine = hit.transform.parent.GetComponent<Line>();
                
                int orderNumber = pointList.IndexOf(_selectedLine.GetPointB());

                for (int i = 0; i < pointList.Count -1; i++)
                {
                    if (!pointList[i].gameObject.activeInHierarchy)
                    {
                        pointList[i].transform.position = new Vector3(hit.point.x, CustomLineRenderer.transform.position.y + 0.001f, hit.point.z);
                        pointList[i].transform.SetSiblingIndex(orderNumber);
                        pointList[i].gameObject.SetActive(true);
                        break;
                    }
                }

                newSortedList.Clear();
                newSortedList.AddRange(_pointContainer.GetComponentsInChildren<TouchPoint>());

                for (int i = 0; i < newSortedList.Count; i++)
                {
                    pointList[i] = newSortedList[i];
                }

                CustomLineRenderer.DrawLines(pointList);

                return;
            }
            if (Input.GetMouseButtonDown(0))
            {
                AddPoint(hit.point);
                DebugPlaneMesh.SetVerticesData(hit.point);
            }
        }
    }

    private void AddPoint(Vector3 position)
    {
        for (int i = 0  ; i < pointList.Count - 1; i++)
        {
            if(!pointList[i].gameObject.activeInHierarchy)
            {
                pointList[i].gameObject.SetActive(true);
                pointList[i].transform.SetParent(_pointContainer);
                pointList[i].transform.position = new Vector3(position.x, CustomLineRenderer.transform.position.y + 0.001f, position.z);
                CustomLineRenderer.DrawLines(pointList);
                break;
            }
        }
    }

    private void CreatePoints()
    {
        for (int i = 0; i < 50; i++)
        {
            TouchPoint point = Instantiate(_point) as TouchPoint;
            point.transform.SetParent(_pointContainer);
            point.gameObject.SetActive(false);
            pointList.Add(point);
        }
    }

    private void UpdateMesh()
    {
        if(Input.GetKeyDown(KeyCode.R))
            CustomLineRenderer.DrawLines(pointList);
    }
    
}
