﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sebastian.Geometry;

public class DebugPlaneMesh : MonoBehaviour
{
    private Mesh _mesh;
    private Mesh _meshData;
    private Shape _shape;
    private CompositeShape compShape;
    private List<Shape> shapes = new List<Shape>();

    [SerializeField]
    private List<Vector3> vertices = new List<Vector3>();

    public void Start()
    {
        _mesh = GetComponent<MeshFilter>().mesh;
        _shape = new Shape();
        shapes.Add(_shape);
        compShape = new CompositeShape(shapes);
    }

    public void SetVerticesData(Vector3 verticle)
    {
        vertices.Add(verticle);
        _shape.points.Add(verticle);
        _meshData = compShape.GetMesh();

        UpdateMesh();
    }

    public void UpdateMesh()
    {
        _mesh.Clear();
        _mesh.vertices = _meshData.vertices;
        _mesh.triangles = _meshData.triangles;
        _mesh.normals = _meshData.normals;
    }
}
