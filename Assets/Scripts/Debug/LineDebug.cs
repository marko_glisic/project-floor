﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDebug : MonoBehaviour
{
    public GameObject Renderer;
    public float size;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            Renderer.transform.localScale = new Vector3(Renderer.transform.localScale.x, Renderer.transform.localScale.y, size);
        }
    }
}
