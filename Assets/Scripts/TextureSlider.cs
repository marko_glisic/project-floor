﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sebastian.Geometry;

public class TextureSlider : MonoBehaviour
{
    [SerializeField]
    private GameObject _showTexturesButton;

    [SerializeField]
    private List<TextureButton> _buttonsList = new List<TextureButton>();

    [SerializeField]
    private CustomPlaneMesh CustomPlaneMesh;

    [SerializeField]
    private PlaneGenerator _planeGenerator;

    public void SelectTexture(Texture2D _mainTexture, Texture2D _normalTexture, Texture2D _agmTexture, float tileWidth)
    {

        // Set tile size
        CustomPlaneMesh.GetComponent<MeshRenderer>().material.SetFloat("_Size", tileWidth);

        // Set main texture
        CustomPlaneMesh.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", _mainTexture);

        // Set normal map
        CustomPlaneMesh.GetComponent<MeshRenderer>().material.SetTexture("_BumpMap", _normalTexture);

        // Set AO/Gloss/Mask
        CustomPlaneMesh.GetComponent<MeshRenderer>().material.SetTexture("_Agm", _agmTexture);

        _planeGenerator.gameObject.SetActive(true);
        _showTexturesButton.SetActive(true);
        gameObject.SetActive(false);
    }
}
