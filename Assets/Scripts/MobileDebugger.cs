﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MobileDebugger : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    public void PrintDebugMessage(string msg){
        _text.text += msg + "\n";
    }
}
