
using UnityEngine;

public class Emmon : MonoBehaviour
{
     [SerializeField]
    private CanvasGroup canvasGroup;
    public void Hide() 
    {
        canvasGroup.alpha = 0f; //this makes everything transparent
        canvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void Show() 
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }

}