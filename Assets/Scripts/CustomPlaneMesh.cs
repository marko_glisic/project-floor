﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sebastian.Geometry;

public class CustomPlaneMesh : MonoBehaviour
{
    private Mesh _mesh;
    private Mesh _meshData;
    private Shape _shape;
    private CompositeShape compShape;
    private List<Shape> shapes = new List<Shape>();
    private List<Vector3> vertices = new List<Vector3>();

    public void Start()
    {
        _mesh = GetComponent<MeshFilter>().mesh;
        _shape = new Shape();
        shapes.Add(_shape);
        compShape = new CompositeShape(shapes);
    }

    public void SetVerticesData(Vector3 verticle)
    {
        vertices.Add(verticle);
        _shape.points.Add(verticle);
        _meshData = compShape.GetMesh();

        UpdateMesh();
    }

    private void UpdateMesh()
    {
        _mesh.Clear();
        _mesh.vertices = _meshData.vertices;
        _mesh.triangles = _meshData.triangles;
        _mesh.normals = _meshData.normals;
    }

    public void AddLinePoint(int index, Vector3 verticle)
    {
        vertices.Insert(index, verticle);
        _shape.points.Insert(index, verticle);
        _meshData = compShape.GetMesh();
        UpdateMesh();
    }

    public void RecalculateMesh(List<Vector3> vertecies)
    {
        vertices = vertecies;
        _shape.points = vertecies;
        _meshData = compShape.GetMesh();

        UpdateMesh();

        _mesh.RecalculateBounds();
        _mesh.RecalculateNormals();
        _mesh.RecalculateTangents();
    }


    public void SetTexture(float tileWidth, Texture mainTexture, Texture normalTexture, Texture agmTexture)
    {
        // Set tile size
        GetComponent<MeshRenderer>().material.SetFloat("_Size", tileWidth);

        // Set main texture
        GetComponent<MeshRenderer>().material.SetTexture("_MainTex", mainTexture);

        // Set normal map
        GetComponent<MeshRenderer>().material.SetTexture("_BumpMap", normalTexture);

        // Set AO/Gloss/Mask
        GetComponent<MeshRenderer>().material.SetTexture("_Agm", agmTexture);
    }

    public void RotateTexture(int angle)
    {
        float _angle = (float)angle;
        
        GetComponent<MeshRenderer>().material.SetFloat("_Angle", _angle);

        // Set rotation center
        GetComponent<MeshRenderer>().material.SetVector("_RotationCenter", transform.position);
    }
}