﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineChild : MonoBehaviour
{
    [SerializeField]
    private Color _selectedColor;

    [SerializeField]
    private Color _unselectedColor;

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag == "Pointer")
        {
            OnPointerEnter();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.transform.tag == "Pointer")
        {
            OnPointerExit();
        }
    }

    public void OnPointerEnter()
    {
        GetComponent<MeshRenderer>().material.color = _selectedColor;
        PlaneManager.Instance.SetLineBool(true);
        PlaneManager.Instance.SelectLine(this.transform.parent.GetComponent<Line>());
    }

    public void OnPointerExit()
    {
        GetComponent<MeshRenderer>().material.color = _unselectedColor;
        PlaneManager.Instance.SetLineBool(false);
        PlaneManager.Instance.SelectLine(null);
    }
}
