﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    public LineChild Renderer;

    [SerializeField]
    private TextMesh _measureText;

    [SerializeField]
    private TouchPoint _pointA;
    
    [SerializeField]
    private TouchPoint _pointB;

    private int _orderIndex;

    public void MeasureLineLength(Vector3 pointA, Vector3 pointB)
    {
        float distance = Vector3.Distance(pointA, pointB);
        _measureText.text = distance.ToString("F2") + "m";
    }

    public void SetConnectingPoints(TouchPoint pointA, TouchPoint pointB)
    {
        _pointA = pointA;
        _pointB = pointB;
    }

    public void UpdateMesh(float yPosition)
    {
        transform.position = new Vector3(_pointA.transform.position.x, yPosition + 0.0001f, _pointA.transform.position.z) + 
            (new Vector3(_pointB.transform.position.x, yPosition + 0.0001f, _pointB.transform.position.z) - 
            new Vector3(_pointA.transform.position.x, yPosition + 0.0001f, _pointA.transform.position.z)) * 0.50f;

        // Set line direction
        Vector3 direction = _pointA.transform.position - _pointB.transform.position;

        // Get angle from the latest point
        float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;

        // Look at last created point
        transform.eulerAngles = Vector3.up * angle;

        // Stretch line between points
        float distance = Vector3.Distance(_pointA.transform.position, _pointB.transform.position);
        Renderer.transform.localScale = new Vector3(Renderer.transform.localScale.x, Renderer.transform.localScale.y, distance/ 10);

        // Measure line length
        MeasureLineLength(_pointA.transform.position, _pointB.transform.position);
    }

    public void UpdateMeshPoints(TouchPoint pointA, TouchPoint pointB)
    {
        _pointA = pointA;
        _pointB = pointB;
    }

    public void SetOrderIndexInList(int index)
    {
        _orderIndex = index;
    }

    public int GetOrderNumber()
    {
        return _orderIndex;
    }

    public TouchPoint GetPointA()
    {
        return _pointA;
    }

    public TouchPoint GetPointB()
    {
        return _pointB;
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }
}
