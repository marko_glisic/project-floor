// Planar normal mapping for surface shader - Marko Tatalovic 2020

Shader "Triplanar/TileShader" {
    Properties {
		_Size("Size in cm", Range(0.1, 100.0)) = 100.0
		_Angle("Rotation", Range(0.0, 360.0)) = 0.0
		_Offset("Offset", Range(0.0, 0.5)) = 1.0
		_MortarSize("Mortar Size", Range(0.0, 1.0)) = 1.0
		[NoScaleOffset] _MainTex ("Albedo (RGB)", 2D) = "white" {}
        [NoScaleOffset] _BumpMap("Normal Map", 2D) = "bump" {}
		_MortarColor ("Mortar Color", Color) = (0.5, 0.5, 0.5)
        [NoScaleOffset] _Agm("AO/Gloss/Mask", 2D) = "white" {}
		
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			Cull off 
			LOD 200

			CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		#include "UnityStandardUtils.cginc"

		// GLSL modulo x - y * floor(x/y).
		#define gmod(x, y) x - y * floor(x/y)

			float _Size;
			float _Angle;
			float _MortarSize;
			float _Offset;
			sampler2D _MainTex;
			sampler2D _BumpMap;
			float4 _MortarColor;
			sampler2D _Agm;

			struct Input {
				float3 worldPos;
				float3 worldNormal;
				float2 uv_MainTex;

				INTERNAL_DATA
			};

			float3 WorldToTangentNormalVector(Input IN, float3 normal) {
				float3 t2w0 = WorldNormalVector(IN, float3(1,0,0));
				float3 t2w1 = WorldNormalVector(IN, float3(0,1,0));
				float3 t2w2 = WorldNormalVector(IN, float3(0,0,1));
				float3x3 t2w = float3x3(t2w0, t2w1, t2w2);
				return normalize(mul(t2w, normal));
			}

			float remap(float a, float b, float c, float d, float t) {
				return c + (t - a) * (d - c) / (b - a);
			}

			float2 Rotate(float2 UV, float2 Center, float Rotation)
			{
				Rotation = Rotation * (3.1415926f / 180.0f);
				UV -= Center;
				float s = sin(Rotation);
				float c = cos(Rotation);
				float2x2 rMatrix = float2x2(c, -s, s, c);
				rMatrix *= 0.5;
				rMatrix += 0.5;
				rMatrix = rMatrix * 2 - 1;
				UV.xy = mul(UV.xy, rMatrix);
				UV += Center;
				return UV;
			}

			void surf (Input IN, inout SurfaceOutputStandard o) {
				// work around bug where IN.worldNormal is always (0,0,0)!
				IN.worldNormal = WorldNormalVector(IN, float3(0,0,1));

				float size = 100.0 / _Size;

				float2 uv = IN.worldPos.xz * size;

				uv = Rotate(uv, 0.0, _Angle);

				// Generate mipmap levels since we are breaking UV continuity with frac(uv)
				float tLOD = log(length(fwidth(uv * 0.75 * 512.0)));

				// Brick Shape
				uv.x += step(1.0, gmod(uv.y, 2.0)) * _Offset;
			
				uv = frac(uv);

				// Mortar Shape
				float MortarSize = remap(0.0, 1.0, 0.0, 0.005, _MortarSize);
				uv.x = remap(0.0, 1.0, 0.0 + MortarSize, 1.0 -MortarSize, uv.x);
				uv.y = remap(0.0, 1.0, 0.0 + MortarSize, 1.0 - MortarSize, uv.y);
									
				float3 agm = tex2Dlod(_Agm, float4(uv, 0, tLOD)).rgb;
				
				float4 diffuseColor = tex2Dlod(_MainTex, float4(uv, 0, tLOD));
				diffuseColor = lerp( _MortarColor, diffuseColor, agm.b);

				// set surface ouput properties
				o.Albedo = diffuseColor;
				o.Metallic = 0.0;
				o.Smoothness = 1.0 - agm.g;
				o.Occlusion = agm.r;

				// tangent space normal maps
				half3 tnormal = UnpackNormal(tex2Dlod(_BumpMap, float4(uv, 0, tLOD)));

				// convert world space normals into tangent normals
				o.Normal = WorldToTangentNormalVector(IN, tnormal.xzy); //swizzle tangent normals to match world normal and blend together

				}
        ENDCG
    }
    FallBack "Diffuse"
}
