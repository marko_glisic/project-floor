// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Planar normal mapping for surface shader - Marko Tatalovic 2020

Shader "Triplanar/NewTileShader" {
    Properties {
		_Size("Size in cm", Range(15.0, 100.0)) = 100.0
		[MaterialToggle] _is2x1("2:1", Float) = 0
		_Angle("Rotation", Range(0.0, 360.0)) = 0.0
		_Offset("Offset", Range(0.0, 0.5)) = 1.0
		_MortarSize("Mortar Size", Range(0.0055, 0.000)) = 0.0055
		[NoScaleOffset] _MainTex ("Albedo (RGB)", 2D) = "white" {}
        [NoScaleOffset] _BumpMap("Normal Map", 2D) = "bump" {}
		_MortarColor ("Mortar Color", Color) = (0.784, 0.784, 0.784)
        [NoScaleOffset] _Agm("AO/Gloss/Mask", 2D) = "white" {}
		_Gloss("Gloss", Range(0.0, 1.0)) = 0.0
		
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			Cull off
			LOD 400

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard fullforwardshadows 

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			#include "UnityStandardUtils.cginc"


			#define MORTAR_SIZE 0.0055
			// GLSL modulo x - y * floor(x/y).
			#define gmod(x, y) x - y * floor(x/y)



			float _Size;
			float _is2x1;
			float _Angle;
			float _MortarSize;
			float _Offset;
			sampler2D _MainTex;
			sampler2D _BumpMap;
			float4 _MortarColor;
			sampler2D _Agm;
			float _Gloss;

			struct Input {
				float3 worldPos;
				float3 worldNormal;
				float2 uv_MainTex;

				INTERNAL_DATA
			};

			float3 WorldToTangentNormalVector(Input IN, float3 normal) {
				float3 t2w0 = WorldNormalVector(IN, float3(1,0,0));
				float3 t2w1 = WorldNormalVector(IN, float3(0,1,0));
				float3 t2w2 = WorldNormalVector(IN, float3(0,0,1));
				float3x3 t2w = float3x3(t2w0, t2w1, t2w2);			
				return normalize(mul(t2w, normal));
			}

			float remap(float a, float b, float c, float d, float t) {
				return c + (t - a) * (d - c) / (b - a);
			}

			float2 Rotate(float2 UV, float2 Center, float Rotation)
			{
				Rotation = Rotation * (3.1415926f / 180.0f);
				UV -= Center;
				float s = sin(Rotation);
				float c = cos(Rotation);
				float2x2 rMatrix = float2x2(c, -s, s, c);
				UV.xy = mul(UV.xy, rMatrix);
				UV += Center;
				return UV;
			}

			half3 blend_rnm(half3 n1, half3 n2)
			{
				n1.z += 1;
				n2.xy = -n2.xy;

				return n1 * dot(n1, n2) / n1.z - n2;
			}

			void surf (Input IN, inout SurfaceOutputStandard o) {
				// work around bug where IN.worldNormal is always (0,0,0)!
				IN.worldNormal = WorldNormalVector(IN, float3(0,0,1));

				//From size in cm to UV space
				float size = 100.0 / _Size;

				//Tile ratio
				float ratio = _is2x1 + 1;
				
				//UV Scale
				float2 uv = IN.worldPos.xz * size;

				//UV rotation
				uv = Rotate(uv, 0.0, _Angle) * float2(1.0, ratio);

				// Generate mipmap levels since we are breaking UV continuity with frac(uv)
				float tLOD = log(length(fwidth(uv * 2048.0)));
				
				//scale the UVs to compesate for mortar size
				uv *=  1.0 - (MORTAR_SIZE - _MortarSize) * 2.0;
				
				// Brick Shape
				uv.x += step(1.0, gmod(uv.y, 2.0)) * _Offset;
				uv = frac(uv);

				// Mortar Shape
				uv.x = remap(0.0, 1.0, 0.0 + _MortarSize, 1.0 - _MortarSize, uv.x);
				uv.y = remap(0.0, 1.0, 0.0 + _MortarSize * ratio , 1.0 - _MortarSize * ratio, uv.y);
	
		
				float3 agm = tex2Dlod(_Agm, float4(uv, 0, tLOD)).rgb;
				
				float4 diffuseColor = tex2Dlod(_MainTex, float4(uv, 0, tLOD));
				diffuseColor = lerp( _MortarColor, diffuseColor, 1.0 - agm.b);

				// set surface ouput properties
				o.Albedo = diffuseColor;
				o.Metallic = 0.0;
				o.Smoothness = (1.0 - agm.g) + _Gloss;
				o.Occlusion = agm.r;

				// tangent space normal maps
				float3 tnormal = UnpackNormal(tex2Dlod(_BumpMap, float4(uv, 0, tLOD)));

				//Rotate normals 
				tnormal.xy = Rotate(tnormal.xy, 0.0, _Angle);

				//Fix tangent and bitangent
				o.Normal = WorldToTangentNormalVector(IN, tnormal.xzy); 
				}
        ENDCG
    }
    FallBack "Diffuse"
}
